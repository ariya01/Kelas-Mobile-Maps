package com.pentilku.gps;

import java.util.List;

/**
 * Created by Ariya on 09/12/2017.
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
