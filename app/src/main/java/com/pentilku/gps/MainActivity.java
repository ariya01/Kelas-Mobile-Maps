package com.pentilku.gps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private LocationManager mylocationManager;
    private LocationListener mylocationListener;
    private Button btn_Ok, btn_tutup, btn_stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mylocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mylocationListener = new locationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mylocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 200, mylocationListener);
        btn_Ok = (Button) findViewById(R.id.On);
        btn_stop = (Button) findViewById(R.id.stop);
        btn_tutup = (Button) findViewById(R.id.tutup);
        Button btn_maps = (Button)findViewById(R.id.maps);
        btn_maps.setOnClickListener(op);
        btn_Ok.setOnClickListener(op);
        btn_tutup.setOnClickListener(op);
        btn_stop.setOnClickListener(op);
        Button btn_gabung = (Button) findViewById(R.id.gabung);
        btn_gabung.setOnClickListener(op);
    }

    View.OnClickListener op = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.On:
                    aktifGPS(true);
                    break;
                case R.id.stop:
                    aktifGPS(false);
                    break;
                case R.id.gabung:
                    Intent haha = new Intent(MainActivity.this,MapsActivity2.class);
                    TextView latgabung = (TextView)findViewById(R.id.lat);
                    String nilailat= latgabung.getText().toString();
                    TextView lnggabung = (TextView)findViewById(R.id.lng);
                    String nilailong = lnggabung.getText().toString();
                    haha.putExtra("latgabung",nilailat);
                    haha.putExtra("longgabung",nilailong);
                    startActivity(haha);
                case R.id.maps:
                    Intent intent = new Intent(MainActivity.this,MapsActivity.class);
                    startActivity(intent);
                    break;
                case R.id.tutup:
                    onDestroy();
                    MainActivity.super.onDestroy();
                    break;
            }
        }
    };

    private void aktifGPS(Boolean onoff) {
        if (onoff) {
            EditText waktu = (EditText) findViewById(R.id.waktu);
            EditText jarak = (EditText) findViewById(R.id.jarak);

            Integer waktunya = Integer.parseInt(waktu.getText().toString());
            Integer jaraknya = Integer.parseInt(jarak.getText().toString());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mylocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, waktunya, jaraknya, mylocationListener);
            Toast.makeText(this, "GPS Aktif", Toast.LENGTH_SHORT).show();
        }
        else
        {
            mylocationManager.removeUpdates(mylocationListener);
            Toast.makeText(this, "GPS Mati", Toast.LENGTH_SHORT).show();
        }
    }


    private class  locationListener implements LocationListener
    {
        private TextView txlat ,txlong;
        @Override
        public void onLocationChanged(Location location) {
            txlat = (TextView)findViewById(R.id.lat);
            txlong = (TextView)findViewById(R.id.lng);

            txlat.setText(String.valueOf(location.getLatitude()));
            txlong.setText(String.valueOf(location.getLongitude()));

            Toast.makeText(MainActivity.this, "GPS Terdeteksi", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }
}
