package com.pentilku.gps;


import java.util.List;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ariya on 09/12/2017.
 */

public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}