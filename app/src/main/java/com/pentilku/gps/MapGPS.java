package com.pentilku.gps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MapGPS extends AppCompatActivity implements OnMapReadyCallback {
    private LocationManager mylocationManager;
    private LocationListener mylocationListener;
    private GoogleMap mMap;
    private Marker marker;
    private int flag=1;
    Button btn_on,btn_off,btn_close,btn_pergi,btn_cari;
    EditText et_long,et_lat,et_zoom,et_nama,et_waktu,et_jarak;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.hibrid :mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);break;
            case R.id.Satelit : mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);break;
            case R.id.none : mMap.setMapType(GoogleMap.MAP_TYPE_NONE);break;
            case R.id.terain : mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);break;
            case R.id.normal: mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_gps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mylocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mylocationListener = new locationListener();
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mylocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mylocationListener);
        btn_on = (Button)findViewById(R.id.btn_on);
        btn_off = (Button)findViewById(R.id.btn_off);
        btn_close = (Button)findViewById(R.id.btn_tutup);
        btn_pergi = (Button)findViewById(R.id.btn_pergi);
        btn_cari = (Button)findViewById(R.id.btn_cari);

        btn_on.setOnClickListener(op);
        btn_close.setOnClickListener(op);
        btn_off.setOnClickListener(op);
        btn_pergi.setOnClickListener(op);
        btn_cari.setOnClickListener(op);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-7.29, 112.79);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10));
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    private class  locationListener implements LocationListener
    {
        EditText lat = (EditText)findViewById(R.id.et_lat);
        EditText lng = (EditText)findViewById(R.id.et_long);
        ArrayList<LatLng> list= new ArrayList<>();
        @Override
        public void onLocationChanged(Location location) {
            if (flag==1)
            {
                LatLng Posisi = new LatLng(location.getLatitude(),location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(Posisi).title("Posisi" + flag));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Posisi,15));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(Posisi,15);
                mMap.animateCamera(cameraUpdate);
                list.add(Posisi);
                flag++;
            }
            else
            {
                LatLng Posisi = new LatLng(location.getLatitude(),location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(Posisi).title("Posisi" + flag));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Posisi,15));
                list.add(Posisi);
                flag++;

                PolylineOptions polylineOptions = new PolylineOptions();
                for (int i = 0 ; i< list.size();i++)
                {
                    polylineOptions.add(list.get(i));
                }
                Polyline polyline = mMap.addPolyline(polylineOptions);
            }
            String longs= String.valueOf(location.getLongitude());
            lat.setText(longs, TextView.BufferType.EDITABLE);
            String lats = String.valueOf(location.getLatitude());
            lng.setText(lats,TextView.BufferType.EDITABLE);
            Toast.makeText(MapGPS.this, "GPS Terdeteksi", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }
    View.OnClickListener op = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_on:
                    if(cekon())
                    {
                        aktifGPS(true);
                    }
                    break;
                case R.id.btn_off:
                    aktifGPS(false);
                    break;
                case R.id.btn_tutup:
                    finish();
                    System.exit(0);
                    break;
                case R.id.btn_pergi:
                    sembunyikan(view);
                    if (cekpergi())
                    {
                        gotoLokasi();
                    }
                    break;
                case R.id.btn_cari:
                    if (cekcari())
                    {
                        gocari();
                    }
                    break;
            }
        }
    };
    private void aktifGPS(Boolean onoff) {
        if (onoff) {
            EditText waktu = (EditText) findViewById(R.id.et_waktu);
            EditText jarak = (EditText) findViewById(R.id.et_distance);

            Integer waktunya = Integer.parseInt(waktu.getText().toString());
            Integer jaraknya = Integer.parseInt(jarak.getText().toString());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mylocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, waktunya, jaraknya, mylocationListener);
            Toast.makeText(this, "GPS Aktif", Toast.LENGTH_SHORT).show();
        }
        else
        {
            mylocationManager.removeUpdates(mylocationListener);
            Toast.makeText(this, "GPS Mati", Toast.LENGTH_SHORT).show();
        }
    }
    private void sembunyikan(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public void gotoLokasi() {

        EditText lat = (EditText) findViewById(R.id.et_lat);
        EditText lg = (EditText) findViewById(R.id.et_long);
        EditText zoom = (EditText) findViewById(R.id.et_zoom);

        Double dblat = Double.parseDouble(lat.getText().toString());
        Double dblong = Double.parseDouble(lg.getText().toString());
        Float dbzoom = Float.parseFloat(zoom.getText().toString());

        Toast.makeText(this, "Move to lat" + dblat + "Long" + dblong, Toast.LENGTH_SHORT).show();
        gotopeta(dblat, dblong, dbzoom);

    }
    private void gotopeta(Double dblat, Double dblong, Float dbzoom) {
        LatLng Lokasibaru = new LatLng(dblat, dblong);
        mMap.addMarker(new MarkerOptions().position(Lokasibaru).title("Marker in" + dblat + dblong));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Lokasibaru, dbzoom));
    }
    private void gocari() {
        EditText tempat = (EditText) findViewById(R.id.et_nama_tempat);
        Geocoder geocoder = new Geocoder(getBaseContext());
        try {
            List<Address> daftar = geocoder.getFromLocationName(tempat.getText().toString(), 1);
            Address address = daftar.get(0);
            String namaAlamat = address.getAddressLine(0);
            Double lintang = address.getLatitude();
            Double bujur = address.getLongitude();
            Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show();
            EditText zoom = (EditText) findViewById(R.id.et_zoom2);
            Float dbzoom = Float.parseFloat(zoom.getText().toString());
            Toast.makeText(this, "pindah ", Toast.LENGTH_SHORT).show();
            gotopeta(lintang, bujur, dbzoom);

            EditText lat = (EditText) findViewById(R.id.et_lat);
            EditText lng = (EditText) findViewById(R.id.et_long);

            lat.setText(lintang.toString());
            lng.setText(bujur.toString());

            Double tujulat = Double.parseDouble(lat.getText().toString());
            Double tujulng = Double.parseDouble(lng.getText().toString());

            Double asallat = -7.28;
            Double asallng = 112.79;

            hitungjarak(tujulat, tujulng, asallat, asallng);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void hitungjarak(Double lat, Double lng, Double asallat, Double asallng) {
        Location asal = new Location("asal");
        Location tujuan = new Location("tujuan");
        asal.setLongitude(asallng);
        asal.setLatitude(asallat);
        tujuan.setLatitude(lat);
        tujuan.setLongitude(lng);
        float jarak = (Float) asal.distanceTo(tujuan) / 1000;
        String jaraknya = String.valueOf(jarak);
        Toast.makeText(this, jaraknya, Toast.LENGTH_SHORT).show();
    }

    private boolean cekon()
    {
        EditText waktu = (EditText) findViewById(R.id.et_waktu);
        EditText jarak = (EditText) findViewById(R.id.et_distance);

        String waktunya = waktu.getText().toString();
        String jaraknya = jarak.getText().toString();

        if (waktunya.equals("")|| waktunya.equals(null))
        {
            waktu.setError("Belum di Isi");
            return false;
        }
        if (jaraknya.equals("")|| jaraknya.equals(null)) {
            jarak.setError("Belum di Isi");
            return false;
        }
        return true;
    }
    private boolean cekpergi()
    {
        EditText lat = (EditText) findViewById(R.id.et_lat);
        EditText lg = (EditText) findViewById(R.id.et_long);
        EditText zoom = (EditText) findViewById(R.id.et_zoom);

        String latnya = lat.getText().toString();
        String lgnya = lg.getText().toString();
        String zoomnya = zoom.getText().toString();

        if (latnya.equals("")|| latnya.equals(null))
        {
            lat.setError("Belum di Isi");
            return false;
        }
        if (lgnya.equals("")|| lgnya.equals(null)) {
            lg.setError("Belum di Isi");
            return false;
        }
        if (zoomnya.equals("")|| zoomnya.equals(null))
        {
            zoom.setError("Belum di Isi");
            return false;
        }
        return true;

    }

    private boolean cekcari()
    {
        EditText zoom = (EditText) findViewById(R.id.et_zoom2);
        EditText tempat = (EditText) findViewById(R.id.et_nama_tempat);
        String zoomnya = zoom.getText().toString();
        String tempatnya = tempat.getText().toString();
        if (tempatnya.equals("")||tempatnya.equals(null))
        {
            tempat.setError("Belum di Isi");
            return false;
        }
        if (zoomnya.equals("")|| zoomnya.equals(null))
        {
            zoom.setError("Belum di Isi");
            return false;
        }
        return true;
    }

}
